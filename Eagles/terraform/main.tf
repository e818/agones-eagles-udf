terraform {

required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.13"

}


provider "kubernetes" {
    config_path = "~/.kube/config"
}

####################################
#    Deploy Banking BackEnd      ###
####################################

resource "kubernetes_deployment" "eagles" {
  metadata {
    name = "eagles"
    labels = {
      app = "eagles"
    }
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "eagles"
      }
    }

    template {
      metadata {
        labels = {
          app = "eagles"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/e818/client"
          name  = "eagles"
          image_pull_policy = "IfNotPresent"
          port {
            container_port = "3389"
           }
          security_context {
            allow_privilege_escalation = true
            privileged = true
          }
        }
        image_pull_secrets {
          name = "eagles-group-secret"
        }
        }
      }
   }
}

resource "kubernetes_service" "eagles" {
  metadata {
    name = "eagles"
    labels = {
      app = "eagles"
      service = "eagles"
    }
  }
  spec {
    selector = {
      app = "eagles"
    }
    port {
      port        = 3389
      target_port = 3389
      node_port = 30389
    }

    type = "NodePort"
  }
}
