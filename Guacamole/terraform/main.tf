terraform {

required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.13"

}


provider "kubernetes" {
    config_path = "~/.kube/config"
}

####################################
#    Deploy Guacd                ###
####################################

resource "kubernetes_deployment" "guacd" {
  metadata {
    name = "guacd"
    labels = {
      app = "guacd"
    }
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "guacd"
      }
    }

    template {
      metadata {
        labels = {
          app = "guacd"
        }
      }

      spec {
        container {
          image = "guacamole/guacd"
          name  = "guacd"
          image_pull_policy = "IfNotPresent"
          port {
            container_port = "4822"
           }
        }
        }
      }
   }
}

resource "kubernetes_service" "guacd" {
  metadata {
    name = "guacd"
    labels = {
      app = "guacd"
      service = "guacd"
    }
  }
  spec {
    selector = {
      app = "guacd"
    }
    port {
      port        = 4822
      target_port = 4822
    }

    type = "ClusterIP"
  }
}

####################################
#    Deploy Guacamole            ###
####################################

#resource "kubernetes_config_map" "guacamole-custom-properties" {
#  metadata {
#    name = "guacamole-custom-properties"
#  }
#
#  data = {
#    "guacamole.properties" = "${file("${path.cwd}/guacamole.properties")}"
#  }
#
#}

#resource "kubernetes_persistent_volume" "guacamole-extensions-pv" {
#  metadata {
#    name = "guacamole-extensions-pv"
#  }
#  spec {
#    capacity = {
#      storage = "5Mi"
#    }
#    access_modes = ["ReadWriteMany"]
#    storage_class_name = "guacamole-extensions-nfs"
#    persistent_volume_source {
#      nfs {
#        path = "/srv/nfs_share/guacamole_extensions"
#        server = "10.1.20.6"
#      }
#    }
#  }
#}

#resource "kubernetes_persistent_volume_claim" "guacamole-extensions-pv-claim" {
#  metadata {
#    name = "guacamole-extensions-pv-claim"
#  }
#  spec {
#    resources {
#      requests = {
#        storage = "5Mi"
#      }
#    }
#    access_modes = ["ReadWriteMany"]
#    storage_class_name = "guacamole-extensions-nfs"
#  }
#}

resource "kubernetes_persistent_volume" "guacamole-pv" {
  metadata {
    name = "guacamole-pv"
  }
  spec {
    capacity = {
      storage = "5Mi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "guacamole-nfs"
    persistent_volume_source {
      nfs {
        path = "/srv/nfs_share/guacamole"
        server = "10.1.20.6"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "guacamole-pv-claim" {
  metadata {
    name = "guacamole-pv-claim"
  }
  spec {
    resources {
      requests = {
        storage = "5Mi"
      }
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "guacamole-nfs"
  }
}


resource "kubernetes_deployment" "guacamole" {
  metadata {
    name = "guacamole"
    labels = {
      app = "guacamole"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "guacamole"
      }
    }

    template {
      metadata {
        labels = {
          app = "guacamole"
        }
      }

      spec {
        container {
          image = "guacamole/guacamole:latest"
          name  = "guacamole"
          image_pull_policy = "Always"
          port {
            container_port = "8080"
            name = "http"
          }
          env {
            name = "GUACD_HOSTNAME"
            value = "guacd"
          }
          env {
            name = "GUACD_PORT"
            value = "4822"
          }
          env {
            name = "POSTGRES_HOSTNAME"
            value = "postgres"
          }
          env {
            name = "POSTGRES_PORT"
            value = "5432"
          }
          env {
            name = "POSTGRES_DATABASE"
            value = "guacamole_db"
          }
          env {
            name = "POSTGRES_USER"
            value = "eagles_user"
          }
          env {
            name = "POSTGRES_PASSWORD"
            value = "eaglesnest"
          }
          env {
            name = "GUACAMOLE_HOME"
            value = "/tmp/guacamole"
          }
          volume_mount {
            name = "guacamole"
            mount_path = "/tmp/guacamole"
          }
        }
        volume {
          name = "guacamole"
          persistent_volume_claim {
            claim_name = "guacamole-pv-claim"
          }
        }
        }
      }
   }
}

resource "kubernetes_service" "guacamole-http" {
  metadata {
    name = "guacamole-http"
    labels = {
      app = "guacamole"
      service = "guacamol-_http"
    }
  }
  spec {
    selector = {
      app = "guacamole"
    }
    port {
      port        = 8080
      target_port = 8080
      node_port = 30880
    }

    type = "NodePort"
  }
}



####################################
#    Deploy PostgreSQL           ###
####################################

resource "kubernetes_config_map" "postgres-config" {
  metadata {
    name = "postgres-config"
  }

  data = {
    POSTGRES_DB             = "guacamole_db"
    POSTGRES_USER              = "eagles"
    POSTGRES_PASSWORD = "eaglesnest"
  }

}

resource "kubernetes_persistent_volume" "postgres-pv" {
  metadata {
    name = "postgres-pv"
  }
  spec {
    capacity = {
      storage = "5Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "postgres-nfs"
    persistent_volume_source {
      nfs {
        path = "/srv/nfs_share/postgres"
        server = "10.1.20.6"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "postgres-pv-claim" {
  metadata {
    name = "postgres-pv-claim"
  }
  spec {
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "postgres-nfs"
  }
}

resource "kubernetes_deployment" "postgres" {
  metadata {
    name = "postgres"
    labels = {
      app = "postgres"
    }
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "postgres"
      }
    }

    template {
      metadata {
        labels = {
          app = "postgres"
        }
      }

      spec {
        container {
          image = "postgres:13.4"
          name  = "postgres"
          image_pull_policy = "IfNotPresent"
          port {
            container_port = "5432"
          }
          env_from {
            config_map_ref {
              name = "postgres-config"
            }
          }
          volume_mount {
            name = "postgredb"
            mount_path = "/var/lib/postgresql/data"
          }
        }
        volume {
          name = "postgredb"
          persistent_volume_claim {
            claim_name = "postgres-pv-claim"
          }
        }
        }
      }
   }
}

resource "kubernetes_service" "postgres" {
  metadata {
    name = "postgres"
    labels = {
      app = "postgres"
      service = "postgres"
    }
  }
  spec {
    selector = {
      app = "postgres"
    }
    port {
      port        = 5432
      target_port = 5432
    }

    type = "ClusterIP"
  }
}
