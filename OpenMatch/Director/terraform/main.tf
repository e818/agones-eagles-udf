terraform {

required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.13"

}


provider "kubernetes" {
    config_path = "~/.kube/config"
}

####################################
#    Deploy Banking BackEnd      ###
####################################

resource "kubernetes_deployment" "agones-openmatch-director" {
  metadata {
    name = "agones-openmatch-director"
    labels = {
      app = "agones-openmatch-director"
    }
    namespace = "agones-system"
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "agones-openmatch-director"
      }
    }

    template {
      metadata {
        labels = {
          app = "agones-openmatch-director"
        }
      }

      spec {
        service_account_name  = "agones-allocator"
        container {
          image = "registry.gitlab.com/e818/agones-openmatch-director"
          name  = "agones-openmatch-director"
          image_pull_policy = "Always"
        }
        image_pull_secrets {
          name = "eagles-group-secret"
        }
        }
      }
   }
}

