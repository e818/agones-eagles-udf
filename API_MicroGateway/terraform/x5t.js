function validate(r) {
    var clientThumbprint = require("crypto")
        .createHash("sha256")
        .update(Buffer.from(r.variables.ssl_client_raw_cert.replace(/(\n|----|-BEGIN|-END| CERTIFICATE-)/gm, ''), 'base64'))
        .digest("base64url");
    return clientThumbprint === r.variables.jwt_cnf_fingerprint ? '1' : '0';
}

export default { validate }