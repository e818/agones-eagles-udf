terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}


provider "docker" {
  host = "unix:///var/run/docker.sock"
 
}


resource "docker_container" "NginxPlus_API_mGW_NAP_L7DoS" {
  name  = "NginxPlus_API_mGW_NAP_L7DoS"
  image = "registry.gitlab.com/f513/nap-dos:latest"
  ports {
    internal = "${lookup(var.int_port, var.env)}"
    external = "${lookup(var.ext_port, var.env)}"
    ip = "${lookup(var.ext_ip_API, var.env)}"
  }
  ports {
    internal = "${var.int_port_TLS}"
    external = "${var.ext_port_TLS}"
    ip = "${lookup(var.ext_ip_API, var.env)}"
  }
  ports {
    internal = "${lookup(var.int_port, var.env)}"
    external = "${lookup(var.ext_port, var.env)}"
    ip = "${lookup(var.int_ip_API, var.env)}"
  }
  ports {
    internal = "${var.int_port_TLS}"
    external = "${var.ext_port_TLS}"
    ip = "${lookup(var.int_ip_API, var.env)}"
  }
  restart = "always"
  dns = ["172.17.0.1"]
  dns_search = ["nginx-udf.internal"]
  hostname = "NginxPlus_API_mGW_NAP_L7DoS"
  privileged = "true"
  volumes {
    host_path      = "${path.cwd}/nginx.conf"
    container_path = "/etc/nginx/nginx.conf"
  }
  volumes {
    host_path      = "${path.cwd}/custom_log_format.json"
    container_path = "/etc/nginx/custom_log_format.json"
  }
  volumes {
    host_path      = "${path.cwd}/updated_ca.crt"
    container_path = "/etc/nginx/updated_ca.crt"
  }
  volumes {
    host_path      = "${path.cwd}/x5t.js"
    container_path = "/etc/nginx/x5t.js"
  }
  volumes {
    host_path      = "${path.cwd}/api.bank.f5lab.crt"
    container_path = "/etc/nginx/api.bank.f5lab.crt"
  }
  volumes {
    host_path      = "${path.cwd}/api.bank.f5lab.key"
    container_path = "/etc/nginx/api.bank.f5lab.key"
  }
}
