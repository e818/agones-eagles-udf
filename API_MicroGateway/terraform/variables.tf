#Define variables
variable "env" {
  description = "env: dev or prod"
  default = "prod"
}

variable "image_name" {
  description = "Image for container."
  default = "registry.gitlab.com/f513/nap-dos:latest"
 }


variable "container_name" {
  type = map(string)
  description = "Name of the container."
  default     = {
    dev  = "NginxPlus_API_mGW_NAP_L7DoS"
    prod = "NginxPlus_API_mGW_NAP_L7DoS"
  }
}

variable "int_port" {
  type = map(string)
  description = "Internal port for container."
  default     = {
    dev  = "80"
    prod = "80"
  }
}

variable "int_port_TLS" {
  description = "Internal port for container TLS."
  default = "443"
}

variable "int_ip_API" {
  type = map(string)
  description = "Internal IP for container."
  default     = {
    dev  = "10.1.20.9"
    prod = "10.1.20.9"
  }
}

variable "ext_port" {
  type        = map(string)
  description = "External port for container."
  default     = {
    dev  = "8080"
    prod = "8080"
  }
}

variable "ext_port_TLS" {
  description = "External port for container TLS."
  default = "443"
}


variable "ext_ip_API" {
  type = map(string)
  description = "External IP for container."
  default     = {
    dev  = "10.1.10.9"
    prod = "10.1.10.9"
  }
}
